var mongojs = require('mongojs');
var dbconfig = require('./dbconfig');

module.exports = mongojs('chat', ['users', 'dialogs']);