"use strict";

var Promise = require('bluebird');
var db = require('./../db/dbmain');

class DB {

    static addUser(user) {
        return new Promise(function(resolve, reject) {
            db.users.insert(user, function (err, doc) {
                if (err) reject(err);
                else resolve(doc)
            })
        })
    }

    static createDialog(users, room) {
        return new Promise(function(resolve, reject) {
            db.dialogs.insert({
                users: users,
                room: 'rm:' + room
            }, function (err, doc) {
                if (err) reject(err);
                else resolve({doc: doc, users: users, room: 'rm:'+room})
            })
        })
    }

    static findUser(user) {
        return new Promise(function(resolve, reject) {
            db.users.findAndModify({
                query: {
                    'self.age': {$in: user.search.age},
                    'self.gender': {$in: user.search.gender},
                    'search.age': user.self.age,
                    'search.gender': user.self.gender
                },
                sort: {waiting: 1},
                update: {$set: {status: 1}},
                new: true
            },  function(err, doc) {
                if(err) reject(err);
                else resolve(doc)
            })
        })
    }

    static updateStatus(usersID) {
        return new Promise(function(resolve, reject) {
            db.users.update({_id: {$in: usersID}, status: 0}, {$set: {status: 1}}, {multi: true}, function(err, doc) {
                if(err) reject(err);
                else resolve(doc)
            })
        })
    }
}

module.exports = DB;