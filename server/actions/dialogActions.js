"use strict";

class Dialog {

    static open(users, room) {
        this.sockets.connected[users[0].socket].join(room);
        this.sockets.connected[users[1].socket].join(room);
    }

}

module.exports = Dialog;