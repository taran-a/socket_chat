var DB_Actions = require('../actions/dbActions');
var Dialog = require('../actions/dialogActions');
var uuid = require('uuid');

module.exports = function(io) {

    io.on('connection', function(socket){
        console.log('User connected: ', socket.id);

        // Search user for dialog
        socket.on('search', function(user){

            var newUser = {
                self: user.self,
                search: user.search,
                socket: socket.id,
                status: 0,
                waiting: Date.now()
            };

            DB_Actions.findUser(newUser)
            .then(function(user) {
                if(!user) return Promise.reject(0);
                return DB_Actions.createDialog([newUser, user], uuid.v1())
            })
            .then(function(dialog) {
                Dialog.open.call(io, dialog.users, dialog.room);
                io.sockets.in(dialog.room).emit('open::dialog', {msg: 'Hello you can speak'})
            })
            .catch(function(err) {
                switch(err) {
                    case 0:
                        DB_Actions.addUser(newUser);
                        break;
                    default:
                        console.error(err.stack)
                }
            })
        });

        socket.on('send::msg', function(data) {
            Object.keys(this.rooms).map(function(room) {
                if(room.indexOf('rm:') > -1) {
                    io.sockets.in(room).emit('new::msg')
                }
            })
        });

        // Disconnect from server
        socket.on('disconnect', function(){
            console.log('User disconnected');
        });
    });
};