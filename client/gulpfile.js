const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
var combiner = require('stream-combiner2');
const concat = require('gulp-concat');
var less = require('gulp-less');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var html2jade = require('gulp-html2jade');
var optionsJade = {nspaces:2};

var path = {
    jsDIST: '../server/public/javascripts',
    stylesDIST: '../server/public/stylesheets',
    htmlDIST: '../server/views',
    htmlSRC: 'src/index.html'
};

gulp.task('html2jade', function(){
    gulp.src(path.htmlSRC)
        .pipe(html2jade(optionsJade))
        .pipe(gulp.dest(path.htmlDIST));
});

gulp.task('build', function () {
    return browserify({entries: 'src/app.js', extensions: ['.js'], debug: true})
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('all.js'))
        .pipe(gulp.dest(path.jsDIST));
});

gulp.task('less', function () {
    var combined = combiner.obj([
        gulp.src('src/styles/styles.less'),
        less(),
        gulp.dest(path.stylesDIST)
    ]);

    combined.on('error', console.error.bind(console));
    return combined;
});

gulp.task('babel', function () {
    return gulp.src('src/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ["es2015", "react"]
        }))
        .pipe(concat('all.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.jsDIST));
});

//gulp.task('server', function () {
//    const express = require('express');
//    const path = require('path');
//    const app = express();
//
//    app.use(express.static(__dirname + '/dist'));
//
//    app.get('*', function (request, response){
//        response.sendFile(path.resolve(__dirname, 'dist', 'index.html'))
//    });
//
//    app.listen(8000)
//});

gulp.task('watch', function () {
    gulp.watch('src/styles/**/*.less', ['less']);
    gulp.watch('src/index.html', ['html2jade']);
    gulp.watch('src/**/*.js', ['build']);
});

gulp.task('default', ['build', 'less', 'watch', 'html2jade']);