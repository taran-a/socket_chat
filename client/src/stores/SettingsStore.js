var alt = require('../alt');
var SettingsActions = require('../actions/SettingsActions');

class SettingsStore {

    constructor() {

        this.bindListeners({
            setOnline: AppActions.SET_ONLINE
        });
    }

    setOnline(state) {
        this.online = state;
    }

}

module.exports = alt.createStore(SettingsStore, 'SettingsStore');
