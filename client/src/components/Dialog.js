import React from 'react'
import _ from 'lodash'
import $ from 'jquery'
import SocketActions from '../actions/SocketActions'

export default class Dialog extends React.Component {

    constructor() {
        super();
        this.inputing = this.inputing.bind(this);
        this.clear = this.clear.bind(this);
        this.send = this.send.bind(this);
        this.state = {
            text: ''
        }
    }

    componentDidMount() {

    }

    inputing(e) {
        this.setState({
            text: e.target.value
        })
    }

    clear() {
        this.setState({
            text: ''
        })
    }

    send() {
        SocketActions.send(this.state.text)
    }

    render() {
        return (
            <div id="Dialog">
                <div className="dialog-holder"></div>
                <div className="clear" onTouchEnd={this.clear}></div>
                <textarea
                    className="text-panel"
                    onChange={this.inputing}
                    value={this.state.text}
                />
                <div className="send" onTouchEnd={this.send}></div>
            </div>
        )
    }
}
