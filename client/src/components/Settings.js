import React from 'react'
import _ from 'lodash'
import $ from 'jquery'
import SocketActions from '../actions/SocketActions'

export default class Settings extends React.Component {

    constructor() {
        super();
        this.changeSelf = this.changeSelf.bind(this);
        this.changeSearchPattern = this.changeSearchPattern.bind(this);
        this.connect = this.connect.bind(this);
        this.state = {
            self: {
                gender: null,
                age: null
            },
            search: {
                gender: [],
                age: []
            }
        }
    }

    componentDidMount() {
        SocketActions.open((data)=>{
            location.hash = '/dialog'
        })
    }

    connect() {
        SocketActions.search({
            self: this.state.self,
            search: this.state.search
        })
    }

    changeSelf(e) {
        var name = e.target.name;
        var self = _.assign(this.state.self, {[name]: +e.target.value});
        this.setState({self: self})
    }

    changeSearchPattern(e) {
        var name = e.target.name;
        var index = this.state.search[name].indexOf(+e.target.value);

        if(index > -1) {
            this.state.search[name].splice(index, 1)
        } else {
            this.state.search[name].push(+e.target.value)
        }
        this.setState(this.state)
    }

    checkSearchPattern() {
        return Object.keys(this.state.search).some((prop) => this.state.search[prop])
    }

    checkSelfData() {
        if(this.state.youGender && this.state.youAge) {
            return false
        } else {
            return true
        }
    }

    render() {
        return <div id="Settings">
            <div className="nav-container">
                <div className="you settings-box">
                    <span className="box-title">ВЫ</span>
                    <input
                        onChange={this.changeSelf}
                        type="radio"
                        id="genderMan"
                        name="gender"
                        defaultValue={1}
                    />
                    <label className="gender" htmlFor="genderMan">М</label>
                    <input
                        onChange={this.changeSelf}
                        type="radio"
                        id="genderWoman"
                        name="gender"
                        defaultValue={0}
                    />
                    <label className="gender" htmlFor="genderWoman">Ж</label>
                    <div className="age-container">
                        <input
                            onChange={this.changeSelf}
                            type="radio"
                            id="age13"
                            name="age"
                            defaultValue="13"
                        />
                        <label className="age-btn right" htmlFor="age13">+13</label>
                        <input
                            onChange={this.changeSelf}
                            type="radio"
                            id="age18"
                            name="age"
                            defaultValue="18"
                        />
                        <label className="age-btn" htmlFor="age18">+18</label>
                        <input
                            onChange={this.changeSelf}
                            type="radio"
                            id="age23"
                            name="age"
                            defaultValue="23"
                        />
                        <label className="age-btn right" htmlFor="age23">+23</label>
                        <input
                            onChange={this.changeSelf}
                            type="radio"
                            id="age28"
                            name="age"
                            defaultValue="28"
                        />
                        <label className="age-btn" htmlFor="age28">+28</label>
                    </div>
                </div>
                <div className="companion settings-box">
                    <span className="box-title">СОБЕСЕДНИК</span>
                    <input
                        onChange={this.changeSearchPattern}
                        type="checkbox"
                        name="gender"
                        id="genderManOther"
                        defaultValue={1}
                    />
                    <label className="gender" htmlFor="genderManOther" name="gender">М</label>
                    <input
                        onChange={this.changeSearchPattern}
                        type="checkbox"
                        name="gender"
                        id="genderWomanOther"
                        defaultValue={0}
                    />
                    <label className="gender" htmlFor="genderWomanOther" name="gender">Ж</label>
                    <div className="age-container">
                        <input
                            onChange={this.changeSearchPattern}
                            type="checkbox"
                            name="age"
                            id="other13"
                            defaultValue={13}

                        />
                        <label className="age-btn right" htmlFor="other13">+13</label>
                        <input
                            onChange={this.changeSearchPattern}
                            type="checkbox"
                            name="age"
                            id="other18"
                            defaultValue={18}

                        />
                        <label className="age-btn" htmlFor="other18">+18</label>
                        <input
                            onChange={this.changeSearchPattern}
                            type="checkbox"
                            name="age"
                            id="other23"
                            defaultValue={23}

                        />
                        <label className="age-btn right" htmlFor="other23">+23</label>
                        <input
                            onChange={this.changeSearchPattern}
                            type="checkbox"
                            name="age"
                            id="other28"
                            defaultValue={28}
                        />
                        <label className="age-btn" htmlFor="other28">+28</label>
                    </div>
                </div>
                <div className="root-actions">
                    <input className="reset-btn root-btn" defaultValue="Сбросить"/>
                    <input
                        disabled={!this.checkSearchPattern() || this.checkSelfData()}
                        className="start-btn root-btn"
                        onClick={this.connect}
                        defaultValue="Общаться"
                    />
                </div>
            </div>
        </div>
    }
}
