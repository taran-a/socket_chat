import React from 'react'

class App extends React.Component {

    constructor() {
        super();
        this.state = {}
    }


    componentDidMount() {

    }

    render() {
        return <div id="Chat">
            {this.props.children}
        </div>
    }
}

module.exports = App;