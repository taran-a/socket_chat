import Reacr from 'react'
import $ from 'jquery'
import Promise from 'bluebird'
import config from '../config'


class ApiActions {

    static get(data) {

        return new Promise(function (resolve, reject) {
            $.ajax({
                method: 'GET',
                url: `${config.url}:${config.port}`,
                data: data,
                success(response){
                    resolve(response);
                },
                error(response){
                    var error = new Error(response);
                    reject(error);
                }
            });
        })
    }

    static post(data) {

        return new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: `${config.url}:${config.port}`,
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                success(response){
                    resolve(response);
                },
                error(response){
                    var error = new Error(response);
                    reject(error)
                }

            });
        })
    }

    static put(data) {

        return new Promise(function (resolve, reject) {
            $.ajax({
                method: 'PUT',
                url: `${config.url}:${config.port}`,
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                success(response){
                    resolve(response);
                },
                error(response){
                    var error = new Error(response);
                    reject(error)
                }
            });
        })
    }
}

module.exports = ApiActions;