import config from '../config'
import io from 'socket.io-client'
var socket = io(config.host);

class SocketActions {

    static search(data) {
        socket.emit('search', data)
    };

    static open(callback) {
        socket.on('open::dialog', callback)
    };

    static send(msg) {
        socket.emit('send::msg', msg)
    };
}

module.exports = SocketActions;