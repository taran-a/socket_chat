import React from 'react'
import { render } from 'react-dom'
import $ from 'jquery'
import { Router, Route, Link, IndexRoute} from 'react-router'
import createHistory from 'history/lib/createHashHistory';
var history = createHistory({queryKey: false});

import App from './components/App.js'
import Settings from './components/Settings.js'
import Dialog from './components/Dialog.js'

$(()=> {
    render((
        <Router history={history}>
            <Route path="/" component={App}>
                <IndexRoute component={Settings}/>
                <Route path='/dialog' component={Dialog}/>
            </Route>
        </Router>
    ), document.getElementById('app-container'))


});



